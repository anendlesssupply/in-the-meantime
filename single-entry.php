<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package In_The_Meantime
 */
  $archive_link = get_post_type_archive_link('entry');
  wp_redirect ( $archive_link );