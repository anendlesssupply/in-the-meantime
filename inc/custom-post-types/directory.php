<?php
add_action( 'init', 'my_entry_cpt' );
function my_entry_cpt() {
  $labels = array(
    'name'               => _x( 'Entries', 'post type general name', 'fsl' ),
    'singular_name'      => _x( 'Entry', 'post type singular name', 'fsl' ),
    'menu_name'          => _x( 'Directory', 'admin menu', 'fsl' ),
    'name_admin_bar'     => _x( 'Entries', 'add new on admin bar', 'fsl' ),
    'add_new'            => _x( 'Add New', 'Entry', 'fsl' ),
    'add_new_item'       => __( 'Add New Entry', 'fsl' ),
    'new_item'           => __( 'New Entry', 'fsl' ),
    'edit_item'          => __( 'Edit Entry', 'fsl' ),
    'view_item'          => __( 'View Entry', 'fsl' ),
    'all_items'          => __( 'All Entries', 'fsl' ),
    'search_items'       => __( 'Search Entries', 'fsl' ),
    'parent_item_colon'  => __( 'Parent Entry:', 'fsl' ),
    'not_found'          => __( 'No Entries found.', 'fsl' ),
    'not_found_in_trash' => __( 'No Entries found in Trash.', 'fsl' )
  );
 
  $args = array(
    'labels'             => $labels,
    'description'        => __( 'Entries', 'fsl' ),
    'public'             => true,
    'publicly_queryable' => true,
    'show_ui'            => true,
    'show_in_menu'       => true,
    'query_var'          => true,
    'rewrite'            => array( 'with_front' => false, 'slug' => 'directory' ),
    'capability_type'    => 'post',
    'has_archive'        => true,
    'hierarchical'       => false,
    'menu_position'      => 5,
    'menu_icon'          => 'dashicons-art',
    'show_in_rest'       => true,
    'supports'           => array( 'title', 'editor', 'excerpt', 'thumbnail' )
  );
 
  register_post_type( 'entry', $args );
}
