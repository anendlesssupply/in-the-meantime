<?php 
function posts_create_directory_taxonomies() {
    $labels = array(
      'name'                       => _x( 'Filters', 'taxonomy general name', 'textdomain' ),
      'singular_name'              => _x( 'Filter', 'taxonomy singular name', 'textdomain' ),
      'search_items'               => __( 'Search Filters', 'textdomain' ),
      'popular_items'              => __( 'Popular Filters', 'textdomain' ),
      'all_items'                  => __( 'All Filters', 'textdomain' ),
      'parent_item'                => null,
      'parent_item_colon'          => null,
      'edit_item'                  => __( 'Edit Filter', 'textdomain' ),
      'update_item'                => __( 'Update Filter', 'textdomain' ),
      'add_new_item'               => __( 'Add New Filter', 'textdomain' ),
      'new_item_name'              => __( 'New Filter Name', 'textdomain' ),
      'separate_items_with_commas' => __( 'Separate filters with commas', 'textdomain' ),
      'add_or_remove_items'        => __( 'Add or remove filters', 'textdomain' ),
      'choose_from_most_used'      => __( 'Choose from the most used filters', 'textdomain' ),
      'not_found'                  => __( 'No filters found.', 'textdomain' ),
      'menu_name'                  => __( 'Filters', 'textdomain' ),
  );

  $args = array(
      'hierarchical'          => true,
      'labels'                => $labels,
      'show_ui'               => true,
      'show_admin_column'     => true,
      //'update_count_callback' => '_update_post_term_count',
      'query_var'             => true,
      'rewrite'               => array( 'slug' => 'filter' ),
      'show_in_rest'       => true,
    );

  register_taxonomy( 'directory_cat', 'entry', $args );
}

add_action( 'init', 'posts_create_directory_taxonomies', 0 );