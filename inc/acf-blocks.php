<?php
function my_register_blocks() {
  if( function_exists('acf_register_block_type') ) {
    
    acf_register_block_type(array(
      'name'              => 'toggle',
      'title'             => __('Toggle', 'fsl'),
      'description'       => __('A custom toggle block.', 'fsl'),
      'category'          => 'formatting',
      'icon'              => 'open-folder',
      'mode'              => 'preview',
      'supports'          => array(
          'align' => true,
          'mode' => false,
          'jsx' => true
      ),
      'render_template' => 'template-parts/blocks/toggle/toggle.php',
    ));

  }
}
add_action('acf/init', 'my_register_blocks');

function add_block_editor_toggle_assets(){
  wp_enqueue_style( 'block-toggle', get_template_directory_uri() . '/template-parts/blocks/toggle/build/toggle.css', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/toggle/build/toggle.css') );
  wp_enqueue_script( 'block-toggle', get_template_directory_uri() . '/template-parts/blocks/toggle/build/toggle-min.js', array(), filemtime(get_stylesheet_directory() .'/template-parts/blocks/toggle/build/toggle-min.js'), true );
}
add_action('enqueue_block_editor_assets', 'add_block_editor_toggle_assets',10,0);