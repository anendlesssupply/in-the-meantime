<?php
/**
 * In The Meantime functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package In_The_Meantime
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.0' );
}

if ( ! function_exists( 'in_the_meantime_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function in_the_meantime_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on In The Meantime, use a find and replace
		 * to change 'in-the-meantime' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'in-the-meantime', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		add_theme_support( 'align-wide' );

		add_theme_support( 'editor-font-sizes', array(
			array(
			  'name'      => __( 'Small', 'fsl' ),
			  'shortName' => __( 'S', 'fsl' ),
			  'size'      => 13,
			  'slug'      => 'small'
			),
			array(
			  'name'      => __( 'Large', 'fsl' ),
			  'shortName' => __( 'L', 'fsl' ),
			  'size'      => 24,
			  'slug'      => 'large'
			)
		  ) );
	  
		  add_theme_support( 'editor-color-palette', array(
			array(
			  'name'  => 'Black',
			  'slug'  => 'black',
			  'color'	=> '#000000',
			),
			array(
			  'name'  => 'White',
			  'slug'  => 'white',
			  'color'	=> '#ffffff',
			),
			array(
			  'name'  => 'Grey',
			  'slug'  => 'grey',
			  'color'	=> '#787878',
			),
			array(
			  'name'  => 'Blue',
			  'slug'  => 'blue',
			  'color'	=> '#3c4b55',
			),
			array(
			  'name'  => 'Light Blue',
			  'slug'  => 'light-blue',
			  'color'	=> '#95cae7',
			),
			array(
			  'name'  => 'Green',
			  'slug'  => 'green',
			  'color'	=> '#0b9500',
			)
		  ) );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'in-the-meantime' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'in_the_meantime_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'in_the_meantime_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function in_the_meantime_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'in_the_meantime_content_width', 640 );
}
add_action( 'after_setup_theme', 'in_the_meantime_content_width', 0 );

/**
 * Disable the emoji's
 */
function disable_emojis() {
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
	remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
	remove_action( 'wp_print_styles', 'print_emoji_styles' );
	remove_action( 'admin_print_styles', 'print_emoji_styles' );	
	remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
	remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );	
	remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
	add_filter( 'tiny_mce_plugins', 'disable_emojis_tinymce' );
}
add_action( 'init', 'disable_emojis' );

/**
 * Filter function used to remove the tinymce emoji plugin.
 * 
 * @param    array  $plugins  
 * @return   array             Difference betwen the two arrays
 */
function disable_emojis_tinymce( $plugins ) {
	if ( is_array( $plugins ) ) {
		return array_diff( $plugins, array( 'wpemoji' ) );
	} else {
		return array();
	}
}

/**
 * Gutenberg scripts and styles
 */
function underscores_enqueue_gutenberg() {
	wp_register_style( 'underscores-gutenberg', get_stylesheet_directory_uri() . '/editor.css', array(), filemtime(get_stylesheet_directory() .'/editor.css'), 'all' );
	wp_enqueue_style( 'underscores-gutenberg' );
	wp_enqueue_script( 'underscores-editor',  get_stylesheet_directory_uri() . '/js/build/editor-min.js', array( 'wp-blocks', 'wp-dom' ), filemtime( get_stylesheet_directory() . '/js/build/editor-min.js' ), true );
}
add_action( 'enqueue_block_editor_assets', 'underscores_enqueue_gutenberg' );

/**
 * Enqueue scripts and styles.
 */
function in_the_meantime_scripts() {
	wp_enqueue_style( 'in-the-meantime-style', get_stylesheet_uri(), array(), _S_VERSION, 'all' );	
	//wp_enqueue_style( 'in-the-meantime-style', get_stylesheet_uri(), array(), filemtime(get_stylesheet_directory() .'/style.css'), 'all' );	
	wp_style_add_data( 'in-the-meantime-style', 'rtl', 'replace' );

	wp_register_script('app', get_template_directory_uri().'/js/build/site-min.js', array(), _S_VERSION, true );
	//wp_register_script('app', get_template_directory_uri().'/js/build/site-min.js', array(), filemtime(get_stylesheet_directory() .'/js/build/site-min.js'), true );
	wp_enqueue_script('app');
}
add_action( 'wp_enqueue_scripts', 'in_the_meantime_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * ACF options
 */
if( function_exists('acf_add_options_page') ) {	
	acf_add_options_page(array(
		'menu_title' => 'Site Settings',
		'page_title' => 'Site Settings',

	));
}

/**
 * Load ACF Blocks.
 */
if( function_exists('acf_register_block_type') ) {
	require get_template_directory() . '/inc/acf-blocks.php';
}

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/remove-comments.php';

/**
 * Register Custom Post Types
 */
require_once( 'inc/custom-post-types/directory.php' );
//require_once( 'inc/custom-post-types/product.php' );
require_once( 'inc/custom-taxonomies/directory-cat.php' );

// Remove Categories and Tags
//add_action('init', 'myprefix_remove_tax');
function myprefix_remove_tax() {
    register_taxonomy('category', array());
    register_taxonomy('post_tag', array());
}