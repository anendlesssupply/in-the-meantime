<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package In_The_Meantime
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php locate_template("assets/svg/symbol-defs.svg", TRUE, TRUE); ?>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#primary"><?php esc_html_e( 'Skip to content', 'in-the-meantime' ); ?></a>

	<header id="masthead" class="site-header">
		<div class="site-branding">
			<?php
			//the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<span class="screen-reader-text"><?php bloginfo( 'name' ); ?></span>
						<span class="site-title-icon" aria-hidden="true">
							<span class="site-title-icon-dot site-title-icon-dot--one"></span>
							<span class="site-title-icon-dot site-title-icon-dot--two"></span>
							<span class="site-title-icon-dot site-title-icon-dot--three"></span>
						</span>
					</a>
				</h1>
				<?php
			else :
				?>
				<p class="site-title">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home">
						<span class="screen-reader-text"><?php bloginfo( 'name' ); ?></span>
						<span class="site-title-icon" aria-hidden="true">
							<span class="site-title-icon-dot site-title-icon-dot--one"></span>
							<span class="site-title-icon-dot site-title-icon-dot--two"></span>
							<span class="site-title-icon-dot site-title-icon-dot--three"></span>
						</span>
					</a>
				</p>
				<?php
			endif; ?>
		</div><!-- .site-branding -->

		<nav class="site-navigation">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'mobile-menu',
					'fallback_cb' => false,
					'container' => ''
				)
			);
			global $wp;
			$current_url = home_url( $wp->request ) . '/';
			
			$top_nav = get_field('top_navigation', 'option');
			if( $top_nav ): 
				$link = $top_nav['link']; 
				$link_text = $top_nav['link_text'] ?: "Link"; 
				if($link): ?>
					<div class="site-navigation-desktop site-navigation-top <?php if($current_url === $link): echo 'site-navigation-desktop--is-active'; endif; ?>">
						<a href="<?php echo esc_url( $link ); ?>">
							<svg aria-hidden="true" class="icon">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-pdf"></use>
							</svg>
							<span class="sep" aria-hidden="true">&ndash;</span>
							<span><?php echo $link_text; ?></span>
						</a>
					</div>
				<?php endif; 
			endif;

			$left_nav = get_field('left_navigation', 'option');
			if( $left_nav ): 
				$link = $left_nav['link']; 
				$link_text = $left_nav['link_text'] ?: "Link"; 
				if($link): ?>
					<div class="site-navigation-desktop site-navigation-left <?php if($current_url === $link): echo 'site-navigation-desktop--is-active'; endif; ?>">
						<a href="<?php echo esc_url( $link ); ?>">
							<svg aria-hidden="true" class="icon">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-directory"></use>
							</svg>
							<span class="sep" aria-hidden="true">&ndash;</span>
							<span><?php echo $link_text; ?></span>
						</a>
					</div>
				<?php endif; 
			endif;

			$right_nav = get_field('right_navigation', 'option');
			if( $right_nav ): 
				$link = $right_nav['link']; 
				$link_text = $right_nav['link_text'] ?: "Link"; 
				if($link): ?>
					<div class="site-navigation-desktop site-navigation-right <?php if($current_url === $link): echo 'site-navigation-desktop--is-active'; endif; ?>">
						<a href="<?php echo esc_url( $link ); ?>">
							<svg aria-hidden="true" class="icon">
								<use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#icon-plus"></use>
							</svg>
							<span class="sep" aria-hidden="true">&ndash;</span>
							<span><?php echo $link_text; ?></span>
						</a>
					</div>
				<?php endif; 
			endif;
			?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->
