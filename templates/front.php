<?php
/**
 * Template Name: Front page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package In_The_Meantime
 */

get_header();

	while ( have_posts() ) :
		the_post();

		$artwork_by_label = get_field('artwork_by_label');
		if($artwork_by_label): ?>
			<div class="artwork-by-label" aria-hidden="true"><span><?php echo $artwork_by_label; ?></span></div>
		<?php endif; ?>

		<main id="primary" class="site-main">

			<?php get_template_part( 'template-parts/content', 'page-front' ); ?>
			
		</main><!-- #main -->
	<?php endwhile; ?>

<?php
get_footer();
