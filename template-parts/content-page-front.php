<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package In_The_Meantime
 */

?>

<?php 
$footer_text = get_field('footer_text'); 
$main_title_a = get_field('main_title_a') ?: "In the";
$main_title_b = get_field('main_title_b') ?: "meantime..."; ?>
<div class="text-on-a-path-container text-on-a-path-container--title text-on-a-path-container--title-1" data-speed-adjust="16">
    <div class="text-on-a-path__text"><?php echo $main_title_a; ?></div>
    <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 283 230">
        <path class="text-on-a-path__path" d="M107.730896,10.4217996 C75.5248958,22.9457996 82.1358958,43.9467996 32.4278958,66.7617996 C-17.2801042,89.5747996 3.97989584,129.8178 15.7218958,160.0098 C28.2928958,192.3388 43.7108958,253.5118 98.6148958,218.8038 C153.518896,184.0948 155.232896,219.5628 184.617896,208.1348 C256.664896,180.1168 297.858896,113.1448 276.626896,58.5477996 C265.133896,28.9927996 222.526896,76.3857996 199.084896,34.0787996 C175.643896,-8.22820041 140.775896,-2.42920041 107.730896,10.4217996 Z"></path>
    </svg>
</div>

<div class="text-on-a-path-container text-on-a-path-container--title text-on-a-path-container--title-2" data-speed-adjust="14">
    <div class="text-on-a-path__text"><?php echo $main_title_b; ?></div>
    <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 285 215">
        <path class="text-on-a-path__path" d="M125.605321,212.898848 C90.4743211,208.117848 57.1653211,196.286848 57.1743211,147.918848 C57.1833211,99.5508482 -3.05867894,120.346848 1.21732106,88.9228482 C9.11732106,30.8798482 77.6153211,-7.72515184 154.212321,2.69984816 C185.453321,6.95084816 169.758321,38.8018482 234.605321,35.0648482 C299.451321,31.3268482 283.279321,92.3028482 278.601321,126.673848 C274.232321,158.772848 273.317321,204.276848 218.781321,200.129848 C164.246321,195.985848 159.845321,217.558848 125.605321,212.898848 Z"></path>
    </svg>
</div>
<?php
$artist_a = get_field('artist_a') ?: "";
$artist_b = get_field('artist_b') ?: "";
$artist_c = get_field('artist_c') ?: "";
$artist_d = get_field('artist_d') ?: "";
$artist_e = get_field('artist_e') ?: "";
$artist_f = get_field('artist_f') ?: "";
$artist_g = get_field('artist_g') ?: "";
$artist_a_svg = get_field('artist_a_svg') ?: "";
$artist_b_svg = get_field('artist_b_svg') ?: "";
$artist_c_svg = get_field('artist_c_svg') ?: "";
$artist_d_svg = get_field('artist_d_svg') ?: "";
$artist_e_svg = get_field('artist_e_svg') ?: "";
$artist_f_svg = get_field('artist_f_svg') ?: "";
$artist_g_svg = get_field('artist_g_svg') ?: "";
$is_even = false;
?>
<?php if($artist_a):
    if($artist_a_svg):
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_a_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-1" data-start-adjust="2">
        <div class="text-on-a-path__text"><?php echo $artist_a; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 126 142">
            <path class="text-on-a-path__path" d="M61.3681418,140.030896 C29.5871418,140.030896 -4.97985825,114.430896 0.59614175,77.4618958 C6.17014175,40.4918958 27.5901418,-0.220104224 58.3971418,-0.00176994637 C84.9341418,0.18989578 112.125142,23.3098958 119.153142,54.4288958 C120.285142,59.4378958 132.338142,85.3838958 113.852142,85.3838958 C95.3671418,85.3838958 85.6851418,96.8268958 88.3261418,114.430896 C90.9671418,132.035896 82.6031418,140.030896 61.3681418,140.030896 Z"></path>
        </svg>
    </div>
<?php endif; ?>
<?php if($artist_b): 
    if($artist_b_svg): 
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_b_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-2" data-start-adjust="1">
        <div class="text-on-a-path__text"><?php echo $artist_b; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 123 149">
            <path class="text-on-a-path__path" d="M37.6314556,136.538996 C21.5447915,124.310547 15.5467446,113.216797 10.3162759,99.7636719 C5.08580715,86.3105469 7.05260402,82.3529246 13.6658853,74.6657201 C20.2791665,66.9785156 8.84361965,49.1914063 4.87291652,39.5742188 C0.902213396,29.9570313 -3.27552098,13.3554688 10.3162759,9.19921875 C17.9207013,6.87385345 25.3591057,6.60138293 33.1248696,5.03515625 C39.2392717,3.80198207 44.9281833,1 55.3533853,1 C79.0194009,1 96.2947915,16.7128906 110.826042,39.5742188 C125.357292,62.4355469 125.357292,100.273504 115.058463,118.40625 C104.759635,136.538996 85.0244959,148.33144 72.8397134,148.33144 C60.6549309,148.33144 53.7181196,148.767445 37.6314556,136.538996 Z"></path>
        </svg>
    </div>
<?php endif; ?>
<?php if($artist_c): 
    if($artist_c_svg): 
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_c_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-3" data-start-adjust="3"  data-speed-adjust="14">
        <div class="text-on-a-path__text"><?php echo $artist_c; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 175 82">
            <path class="text-on-a-path__path" d="M96.4374856,79.5258542 C56.0614856,74.0318542 100.796486,52.2668542 27.3744856,39.8678542 C-46.0475144,27.4678542 52.1834856,-26.7101458 99.4654856,19.0618542 C146.748486,64.8348542 175.819486,43.3578542 173.586486,59.7648542 C171.353486,76.1728542 136.812486,85.0208542 96.4374856,79.5258542 Z"></path>
        </svg>
    </div>
<?php endif; ?>
<?php if($artist_d): 
    if($artist_d_svg): 
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_d_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-4"  data-speed-adjust="12">
        <div class="text-on-a-path__text"><?php echo $artist_d; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 97 124">
            <path class="text-on-a-path__path" d="M5.29259399,119.929555 C-13.447406,107.990555 36.979594,55.5495551 8.43359399,40.1205551 C-20.113406,24.6905551 60.696594,-8.3934449 79.437594,3.5455551 C98.177594,15.4835551 105.490594,65.0165551 76.296594,83.3545551 C47.101594,101.691555 24.032594,131.867555 5.29259399,119.929555 Z"></path>
        </svg>
    </div>
<?php endif; ?>
<?php if($artist_e): 
    if($artist_e_svg): 
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_e_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-5" data-start-adjust="1">
        <div class="text-on-a-path__text"><?php echo $artist_e; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 155 61">
            <path class="text-on-a-path__path" d="M77.709,59.535 C35.344,59.535 1,46.432 1,30.268 C1,14.104 35.344,1 77.709,1 C120.074,1 154.419,14.104 154.419,30.268 C154.419,46.432 120.074,59.535 77.709,59.535 Z"></path>
        </svg>
    </div>
<?php endif; ?>
<?php if($artist_f): 
    if($artist_f_svg): 
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_f_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-6" data-start-adjust="1"  data-speed-adjust="10">
        <div class="text-on-a-path__text"><?php echo $artist_f; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 198 112">
            <path class="text-on-a-path__path" d="M89.4147116,77.1204934 C64.9737116,39.9644934 -4.83928844,40.7304934 1.39171156,22.3774934 C7.62071156,4.02449344 78.1507116,-14.5545066 109.203712,21.9154934 C140.256712,58.3864934 211.236712,50.8124934 194.454712,87.9144934 C177.672712,125.016493 113.857712,114.276493 89.4147116,77.1204934 Z"></path>
        </svg>
    </div>
<?php endif; ?>
<?php if($artist_g): 
    if($artist_g_svg): 
        if($is_even):
            echo '<div class="mobile-text-wrapper mobile-text-wrapper--even">';
            $is_even = false;
        else:
            echo '<div class="mobile-text-wrapper">';
            $is_even = true;
        endif;
        echo $artist_g_svg; 
        echo '</div>'; 
    endif; ?>
    <div class="text-on-a-path-container text-on-a-path-container--artist text-on-a-path-container--artist-7" data-start-adjust="4">
        <div class="text-on-a-path__text"><?php echo $artist_g; ?></div>
        <svg aria-hidden="true" class="text-on-a-path__svg" viewBox="0 0 175 50">
            <path class="text-on-a-path__path" d="M86.859,47.4257799 C39.44,47.4257799 1,36.2317799 1,22.4227799 C1,8.61477989 54.63,-6.92922011 86.859,5.60877989 C119.088,18.1457799 162.898,-11.9362201 172.718,22.4227799 C182.539,56.7827799 134.278,47.4257799 86.859,47.4257799 Z"></path>
        </svg>
    </div>
<?php endif;
if($footer_text): ?>
    <article class="front-footer-text">
        <p><?php echo $footer_text; ?></p>
    </article>
<?php endif;