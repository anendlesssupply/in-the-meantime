<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package In_The_Meantime
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php $terms = get_the_terms(get_the_ID(), 'directory_cat');
	$prefix = "";
	if($terms):
		echo '<p class="entry-terms">';
			foreach($terms as $term):
				echo '<span>' . $prefix . $term->name . '</span>';
				$prefix = ", ";
			endforeach;
		echo '</p>';
	endif; ?>
	<?php in_the_meantime_post_thumbnail(); ?>
	<header class="screen-reader-text entry-header">
		<?php
		if ( is_singular() ) :
			the_title( '<h1 class="entry-title">', '</h1>' );
		else :
			the_title( '<h2 class="entry-title">', '</h2>' );
		endif;
        ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		the_content(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'in-the-meantime' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				wp_kses_post( get_the_title() )
			)
		);
		?>
	</div><!-- .entry-content -->
	<button class="toggle-entry text-button" type="button" title="Toggle entry">More</button>
	<?php $border_styles = ['1','2','3','4','5','6','7'];
	shuffle($border_styles); ?>
    <div class="entry-border <?php echo 'entry-border--' . $border_styles[0]; ?>"></div>
    <?php /*
	<footer class="entry-footer">
		<?php in_the_meantime_entry_footer(); ?>
	</footer><!-- .entry-footer -->
    */ ?>
</article><!-- #post-<?php the_ID(); ?> -->
