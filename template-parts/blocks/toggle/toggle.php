<?php
$id = 'wp-block-acf-toggle-' . $block['id'];
if( !empty($block['anchor']) ) {
    $id = $block['anchor'];
}
$className = 'wp-block-acf-toggle';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}
if( !empty($block['align']) ) {
    $className .= ' align' . $block['align'];
}

$label = get_field('label') ? get_field('label') : 'More';

$template = array(
    array( 'core/heading', array(
        'placeholder' => 'Add a heading',
    ) ),
);
?>
<div id="<?php echo esc_attr($id); ?>" class="<?php echo esc_attr($className); ?>">
    <button class="toggle-button" aria-controls="<?php echo 'toggle-content-' . $id; ?>" aria-expanded="false">
        <span class="toggle-button__label"><?php echo $label; ?></span>
        <span class="toggle-button__icon" aria-hidden="true">
            <svg width="100%" height="100%" viewBox="0 0 24 24">
                <path d="M15 5.016l-1.406 1.406 4.594 4.594h-16.172v1.969h16.172l-4.594 4.594 1.406 1.406 6.984-6.984z"></path>
            </svg>
        </span>
    </button>
    <div id="<?php echo 'toggle-content-' . $id; ?>" class="toggle-content" aria-expanded="false">
        <InnerBlocks template="<?php esc_attr( wp_json_encode( $template ) ); ?>" />
    </div>
</div>